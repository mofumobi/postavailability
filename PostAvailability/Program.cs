﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Models;

namespace PostAvailability
{
    class Program
    {
        private const string username = "USERNAME_HERE";
        private const string password = "PASSWORD_HERE";
        static void Main(string[] args)
        {
            var authData = Authorize().Result;
            PostItems(authData).Wait();
            PostAvailability(authData).Wait();
            DeleteItems(authData).Wait();
            DeleteAvailability(authData).Wait();
            Console.ReadLine();
        }

        private static async Task PostItems(AuthRequest authData)
        {
            var itemUri = $"https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload";
            var items = new ItemUpload();
            var products = new List<Product>();
            products.Add(new Product
            {
                ProductId = "364729",
                Genus = "Bixa",
                IsActive = true,
                IsAlwaysAvailable = true,
                Name = "Bixa americana Poir.",
                Size = "SEED",
                Brand = "Brand",
                Category = "Annuals",
                Type = "Bixa"
            });
            items.Items = products.ToArray();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            var availabilityResponse = await client.PostAsXmlAsync(itemUri, items);
            Console.WriteLine($"Items Response: {availabilityResponse.StatusCode}");
        }

        private static async Task DeleteItems(AuthRequest authData)
        {
            var itemUri = $"https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            var availabilityResponse = await client.DeleteAsync(itemUri);
            Console.WriteLine($"Delete Items Response: {availabilityResponse.StatusCode}");
        }

        private static async Task DeleteAvailability(AuthRequest authData)
        {
            var availabilityUri = $"https://vendoravailability-qa.azurewebsites.net/api/v2/availabilityupload";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            var availabilityResponse = await client.DeleteAsync(availabilityUri);
            Console.WriteLine($"Delete Availability Response: {availabilityResponse.StatusCode}");
        }

        private static async Task PostAvailability(AuthRequest authData)
        {
            var availabilityUri = $"https://vendoravailability-qa.azurewebsites.net/api/v2/availabilityupload";
            var availability = new AvailabilityUpload();
            var itemWeeks = new List<ItemWeek>();
            itemWeeks.Add(new ItemWeek
            {
                ProductID = "364729",
                Quantity = 14800,
                ShipWeek = 201715,
                VendorID = "0000000040"
            });
            availability.ItemWeeks = itemWeeks.ToArray();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            var sw = Stopwatch.StartNew();
            var availResponse = Enumerable.Range(0, 14).Select(m =>
            {
                var bytes = File.ReadAllBytes($@"Data\AvailabilityUpload{m}.xml");
                var content = new ByteArrayContent(bytes);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(@"application/xml");
                return client.PostAsync(availabilityUri, content);
            });
            var responses = await Task.WhenAll(availResponse);
            sw.Stop();
            Console.WriteLine($"Response Duration: {sw.Elapsed}");
            responses.ToList().ForEach(m => Console.WriteLine($"Loop Availability Response: {m.StatusCode}"));
            var availabilityResponse = await client.PostAsXmlAsync(availabilityUri, availability);
            Console.WriteLine($"Availability Response: {availabilityResponse.StatusCode}");
        }

        private static async Task<AuthRequest> Authorize()
        {
            var tokenUri = $"https://vendoravailability-qa.azurewebsites.net/token";
            var client = new HttpClient();
            var authDictionary = new Dictionary<string, string>();
            authDictionary.Add("username", username);
            authDictionary.Add("password", password);
            authDictionary.Add("grant_type", "password");
            var auth = new FormUrlEncodedContent(authDictionary);
            var authResponse = await client.PostAsync(tokenUri, auth);
            authResponse.EnsureSuccessStatusCode();
            return await authResponse.Content.ReadAsAsync<AuthRequest>();
        }
    }
}
