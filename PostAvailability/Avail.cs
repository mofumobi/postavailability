﻿using System.Runtime.Serialization;

namespace PostAvailability
{
    public class Avail
    {
        public ProductItem[] value { get; set; }
    }

    public class ProductItem
    {
        public string ItemCode { get; set; }
        public string Name { get; set; }
        public AvailabilityRecord[] Availability { get; set; }
    }

    public class AvailabilityRecord
    {
        public decimal Quantity { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
    }

}