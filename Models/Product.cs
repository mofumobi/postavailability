﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [DataContract(Namespace ="")]
    public partial class Product
    {

        private string genusField;

        private bool isAlwaysAvailableField;

        private string productIdField;

        private string nameField;

        private string sizeField;

        private bool isActiveField;

        private string brandField;

        /// <remarks/>
        [DataMember]
        public string Genus
        {
            get
            {
                return this.genusField;
            }
            set
            {
                this.genusField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public bool IsAlwaysAvailable
        {
            get
            {
                return this.isAlwaysAvailableField;
            }
            set
            {
                this.isAlwaysAvailableField = value;
            }
        }

        [DataMember]
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ProductId
        {
            get
            {
                return this.productIdField;
            }
            set
            {
                this.productIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        [DataMember]
        public string Brand
        {
            get
            {
                return this.brandField;
            }
            set
            {
                this.brandField = value;
            }
        }

        public string Type { get; set; }
        public string Category { get; set; }
    }
}
