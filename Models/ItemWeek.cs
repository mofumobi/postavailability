﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    /// <remarks/>
    [System.Xml.Serialization.XmlType(AnonymousType = true, Namespace = "")]
    [DataContract(Namespace ="")]
    public partial class ItemWeek
    {

        private string productIDField;

        private decimal quantityField;

        private uint shipWeekField;

        private string vendorIDField;

        /// <remarks/>
        [DataMember]
        public string ProductID
        {
            get
            {
                return this.productIDField;
            }
            set
            {
                this.productIDField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public decimal Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public uint ShipWeek
        {
            get
            {
                return this.shipWeekField;
            }
            set
            {
                this.shipWeekField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string VendorID
        {
            get
            {
                return this.vendorIDField;
            }
            set
            {
                this.vendorIDField = value;
            }
        }
    }
}
