﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <remarks/>
    [DataContract(Namespace = "")]
    public partial class PurchaseOrder
    {

        private string brokerIdField;

        private string clientIdField;

        private DateTimeOffset createdDateField;

        private string customerPONumberField;

        private Load[] loadsField;

        private string orderIdField;

        private int revisionField;

        private string revisionIdField;

        private string tagRatioField;

        [DataMember]
        /// <remarks/>
        public string BrokerId
        {
            get
            {
                return this.brokerIdField;
            }
            set
            {
                this.brokerIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ClientId
        {
            get
            {
                return this.clientIdField;
            }
            set
            {
                this.clientIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public DateTimeOffset CreatedDate
        {
            get
            {
                return this.createdDateField;
            }
            set
            {
                this.createdDateField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string CustomerPONumber
        {
            get
            {
                return this.customerPONumberField;
            }
            set
            {
                this.customerPONumberField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public Load[] Loads
        {
            get
            {
                return this.loadsField;
            }
            set
            {
                this.loadsField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string OrderId
        {
            get
            {
                return this.orderIdField;
            }
            set
            {
                this.orderIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public int Revision
        {
            get
            {
                return this.revisionField;
            }
            set
            {
                this.revisionField = value;
            }
        }

        [DataMember]
        public string RevisionId
        {
            get
            {
                return this.revisionIdField;
            }
            set
            {
                this.revisionIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string TagRatio
        {
            get
            {
                return this.tagRatioField;
            }
            set
            {
                this.tagRatioField = value;
            }
        }
    }

    /// <remarks/>
    [DataContract(Namespace = "")]
    public class Load
    {

        private string address1Field;

        private string address2Field;

        private string addressCodeField;

        private string careOfField;

        private string cityField;

        private string countryField;

        private PurchaseOrderItem[] itemsField;

        private string phoneField;

        private string postalCodeField;

        private string stateField;

        private string vendorCodeField;

        /// <remarks/>
        [DataMember]
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string AddressCode
        {
            get
            {
                return this.addressCodeField;
            }
            set
            {
                this.addressCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string CareOf
        {
            get
            {
                return this.careOfField;
            }
            set
            {
                this.careOfField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public PurchaseOrderItem[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }
    }

    /// <remarks/>
    [DataContract(Namespace = "")]
    public class PurchaseOrderItem
    {
        private bool autoConfirmField;

        private string buyerProductCodeField;

        private string commentField;

        private string confirmationNumberField;

        private string currencyCodeField;

        private DateTimeOffset? cutoffDateField;

        private DateTimeOffset? expirationDateField;

        private decimal? freightCostField;

        private string freightNotesField;

        private int lineIdField;

        private int lineNumberField;

        private decimal? orderedQuantityField;

        private decimal? priceField;

        private string productDescriptionField;

        private string shipViaCodeField;

        private string shipViaDescriptionField;

        private string sizeField;

        private string statusField;

        private int shipWeekField;

        private decimal? taxField;

        private string vendorProductCodeField;

        /// <remarks/>
        [DataMember]
        public bool AutoConfirm
        {
            get
            {
                return this.autoConfirmField;
            }
            set
            {
                this.autoConfirmField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string BuyerProductCode
        {
            get
            {
                return this.buyerProductCodeField;
            }
            set
            {
                this.buyerProductCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ConfirmationNumber
        {
            get
            {
                return this.confirmationNumberField;
            }
            set
            {
                this.confirmationNumberField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public DateTimeOffset? CutoffDate
        {
            get
            {
                return this.cutoffDateField;
            }
            set
            {
                this.cutoffDateField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public DateTimeOffset? ExpirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public decimal? FreightCost
        {
            get
            {
                return this.freightCostField;
            }
            set
            {
                this.freightCostField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string FreightNotes
        {
            get
            {
                return this.freightNotesField;
            }
            set
            {
                this.freightNotesField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public int LineId
        {
            get
            {
                return this.lineIdField;
            }
            set
            {
                this.lineIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public int LineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public decimal? OrderedQuantity
        {
            get
            {
                return this.orderedQuantityField;
            }
            set
            {
                this.orderedQuantityField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public decimal? Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ProductDescription
        {
            get
            {
                return this.productDescriptionField;
            }
            set
            {
                this.productDescriptionField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ShipViaCode
        {
            get
            {
                return this.shipViaCodeField;
            }
            set
            {
                this.shipViaCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string ShipViaDescription
        {
            get
            {
                return this.shipViaDescriptionField;
            }
            set
            {
                this.shipViaDescriptionField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public int ShipWeek
        {
            get
            {
                return this.shipWeekField;
            }
            set
            {
                this.shipWeekField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public decimal? Tax
        {
            get
            {
                return this.taxField;
            }
            set
            {
                this.taxField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string VendorProductCode
        {
            get
            {
                return this.vendorProductCodeField;
            }
            set
            {
                this.vendorProductCodeField = value;
            }
        }
    }
}
