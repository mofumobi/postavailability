﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    /// <remarks/>
    [System.Xml.Serialization.XmlType(AnonymousType = true, Namespace = "")]
    [System.Xml.Serialization.XmlRoot(Namespace = "", IsNullable = false)]
    [DataContract(Namespace ="")]
    public partial class AvailabilityUpload
    {

        private ItemWeek[] itemWeeksField;

        /// <remarks/>
        [DataMember]
        public ItemWeek[] ItemWeeks
        {
            get
            {
                return this.itemWeeksField;
            }
            set
            {
                this.itemWeeksField = value;
            }
        }
    }




}
