# README #

### What is this repository for? ###

* An Example project of how to interact with the SBI Beacon
* 2.0.0
* [Looking for v1 Documentation](https://bitbucket.org/mofumobi/postavailability/wiki/Version%201)

### How do I get set up? ###

* Before running application set your username and password

```
#!c#
private const string username = "USERNAME_HERE";
private const string password = "PASSWORD_HERE";

```

### Example Requests ###

#### Authentication ####
Obtains an authorization token to use to access all other Requests

* Url: https://vendoravailability-qa.azurewebsites.net/token
  * POST to this url
  * Body should be x-www-form-urlencoded
  * Keys
    * username
    * password
    * grant_type


**Example Request**

```
POST https://vendoravailability-qa.azurewebsites.net/token HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 77
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

username=user%40example.com&password=password&grant_type=password
```

**Example Response**
```
HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Length: 668
Content-Type: application/json;charset=UTF-8
Expires: -1
Server: Microsoft-IIS/7.5
Set-Cookie: .AspNet.Cookies=ft8aYiQ1In2pXAOagv1VmBYxkJIDdNWcqVDX_01JVvSZGBo91yDdCZEXm5i-01QBS9Y1r4gLDHPb-8ITy9QYJrUXgcZ4r-sxEjnfDsszs-W7QTVKQwrYBxSjoy2mk2WMsZaVYclEfP9804WEH9ecME1kWBT8RSd-lrmqB6Lzve2aeYMYapii10AC2JzHvaCtt7T3tcGHGIxDINCs0RHaXVhudJmmotOpaNPqrKT_RhoQz-uWzzveJv9RH0nh4t88MvC09-mChtqm2z4UqodBabeqM-s3tSHu931fWZ2lL1F134XA0p7SzSftD3JIvhjO3vrC6QVNU9rx7k2s308g5g3b-pX7pPzpGgBn1ztsQdQuQa1YSrwy-iMS_S-Z2zH5PqsXMrhX-1D6K_DDwVtqnYfmiXO7_E4ecBrpGxROh39RqwccGisnlzBySGJj5q8Ov_6sTav3TtQQLixILGMoxiQhTuShE68MmdxJXdXR2TA; path=/; HttpOnly
X-Powered-By: ASP.NET
Date: Fri, 03 Mar 2017 19:21:50 GMT

{"access_token":"hQZ0x-OB818hh0dWfw9RiHifJTYHiSgEI_1XMSaO7i1w_E5k2IwFhpyethmFaVbI-0dll60p7y1n20-WrjBI_hrJ_aGcxNh3cXI4-jX-qM5R-EussTXfvcfreCHCcbavL9OcyPD0BZblbYlqisawaB2FMwFf32I2gG_-Q5iM2vJpFVi5SmuiH1cAGhyvsxsEM25YD3KzxsdQokrpFR_r-NOTb4ya9qB1ll_wbGTf4rHHL5R3r8qHkS6xMsM9ycclgBV6MfsqThS8dPe2k78a0yEHiU_pnw4Eij0QEXpjpUtyFd8vW8WCFttPRHjowgAfhQf1yuCUr-AwF2AwZja4dFtkxcw30bzqi99p-fehlT5qoUQn-_3pTp39fXx50dKni6lUhzQz2Fq_yr4ciYkpGPc3l3ABM58DlvakXB1vRMP4QF7antKP11I1yIB7A7oZOS7qBeloTngeTjoi1LOfgjOuhv_48ZSgaJu_sKbLJAI","token_type":"bearer","expires_in":1209599,"userNameinfo@eplantsource.com",".issued":"Fri, 03 Mar 2017 19:21:50 GMT",".expires":"Fri, 17 Mar 2017 19:21:50 GMT"}
```

#### Token Usage ####

* The access_token found in the token response is used to authorize future requests
* The access_token is the request Header as the Authorization

#### Upload Availability ####
Allows the ability to set on hand counts for specific products at a specific ShipWeek

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/availabilityupload
* POST to this url
* Header requires Authorization and Content-Type
* Body should contain the AvailabilityUpload xml

**Example Request**
```
POST https://vendoravailability-qa.azurewebsites.net/api/v2/availabilityupload HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 387
Accept: application/atom+xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer hQZ0x-OB818hh0dWfw9RiHifJTYHiSgEI_1XMSaO7i1w_E5k2IwFhpyethmFaVbI-0dll60p7y1n20-WrjBI_hrJ_aGcxNh3cXI4-jX-qM5R-EussTXfvcfreCHCcbavL9OcyPD0BZblbYlqisawaB2FMwFf32I2gG_-Q5iM2vJpFVi5SmuiH1cAGhyvsxsEM25YD3KzxsdQokrpFR_r-NOTb4ya9qB1ll_wbGTf4rHHL5R3r8qHkS6xMsM9ycclgBV6MfsqThS8dPe2k78a0yEHiU_pnw4Eij0QEXpjpUtyFd8vW8WCFttPRHjowgAfhQf1yuCUr-AwF2AwZja4dFtkxcw30bzqi99p-fehlT5qoUQn-_3pTp39fXx50dKni6lUhzQz2Fq_yr4ciYkpGPc3l3ABM58DlvakXB1vRMP4QF7antKP11I1yIB7A7oZOS7qBeloTngeTjoi1LOfgjOuhv_48ZSgaJu_sKbLJAI
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

<AvailabilityUpload>
    <ItemWeeks>
        <ItemWeek>
            <ProductID>364729</ProductID>
            <Quantity>14800</Quantity>
            <ShipWeek>201715</ShipWeek>
            <VendorID>0000000040</VendorID>
        </ItemWeek>
    </ItemWeeks>
</AvailabilityUpload>
```

#### Upload Item Master ####
Allows the ability to Add or Update a list of Products by ProductId

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload
* POST to this url
* Header requires Authorization and Content-Type
* Body should contain the ItemUpload xml

**Example Request**
```
POST https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 331
Accept: application/atom+xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer h5waAZX2IQzP3Vz9DZt2j3PDZ6m78Bw83MfH1we8i795WUkOeU3CloK67uX3zN5tzPAv6AGZGAtrDJjsUxuIUk4oE6u1FrrJTN1WsE8xN5oZ1OTSX5SB0YD214GwlPkUigC6BHDrGDezKcb5VVxFSRsZKheM3vsCfUScikQOuFHSd8Nqf7imITIhJdSwbu-jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

<ItemUpload>
    <Items>
        <Product>
            <Brand>Brand</Brand>
            <Category>Annual</Category>
            <Genus>Bixa</Genus>
            <IsActive>true</IsActive>
            <IsAlwaysAvailable>true</IsAlwaysAvailable>
            <Name>Bixa americana Poir.</Name>
            <ProductId>364729</ProductId>
            <Size>SEED</Size>
            <Type>Bixa</Type>
        </Product>
    </Items>
</ItemUpload>
```

#### Delete Item Master ####

Discontinues all Products

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload
* DELETE to this url
* Header requires Authorization

**Example Request**
```
DELETE https://vendoravailability-qa.azurewebsites.net/api/v2/itemupload HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 0
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer h5waAZX2IQzP3Vz9DZt2j3PDZ6m78Bw83MfH1we8i795WUkOeU3CloK67uX3zN5tzPAv6AGZGAtrDJjsUxuIUk4oE6u1FrrJTN1WsE8xN5oZ1OTSX5SB0YD214GwlPkUigC6BHDrGDezKcb5VVxFSRsZKheM3vsCfUScikQOuFHSd8Nqf7imITIhJdSwbu-jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
```

#### Get New Orders ####
Gets a list of all new unacknowledged orders

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder?onlyNew=true
* GET to this url
* Header requires Authorization and Content-Type

**Example Request**
```
GEThttps://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder?onlyNew=true HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 0
Accept: application/xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer h5waAZX2IQzP3Vz9DZt2j3PDZ6m78Bw83MfH1we8i795WUkOeU3CloK67uX3zN5tzPAv6AGZGAtrDJjsUxuIUk4oE6u1FrrJTN1WsE8xN5oZ1OTSX5SB0YD214GwlPkUigC6BHDrGDezKcb5VVxFSRsZKheM3vsCfUScikQOuFHSd8Nqf7imITIhJdSwbu-jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8
```

**Example Response**
```
HTTP/1.1 200 OK
Content-Type: application/xml; charset=utf-8
Server: Microsoft-IIS/10.0
Request-Context: appId=cid-v1:c23dce53-21ad-4cee-96f6-c55a6eaca160
X-Powered-By: ASP.NET
Date: Wed, 26 Apr 2017 21:07:32 GMT
Content-Length: 2708

<ArrayOfPurchaseOrder xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <PurchaseOrder>
        <BrokerId>95d2c23c-7812-4783-a887-1da495c678ae</BrokerId>
        <ClientId>18abfe7c-903a-4427-9c8c-94dfc3ebf7c8</ClientId>
        <CreatedDate xmlns:d3p1="http://schemas.datacontract.org/2004/07/System">
            <d3p1:DateTime>2017-07-15T20:50:24.7785653Z</d3p1:DateTime>
            <d3p1:OffsetMinutes>-420</d3p1:OffsetMinutes>
        </CreatedDate>
        <CustomerPONumber>PO-1234</CustomerPONumber>
        <Loads>
            <Load>
                <Address1>1077 Cane Ridge Road</Address1>
                <Address2></Address2>
                <AddressCode>1234</AddressCode>
                <CareOf i:nil="true" />
                <City>Paris</City>
                <Country>US</Country>
                <Items>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>ALCCLSD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>1</LineNumber>
                        <OrderedQuantity>1000.0000</OrderedQuantity>
                        <Price>1000.0000</Price>
                        <ProductDescription>Alcea Rosea Spring Celebrities Lemon</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>New Order</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>107831</VendorProductCode>
                    </PurchaseOrderItem>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>AQUBASD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>2</LineNumber>
                        <OrderedQuantity>2000.0000</OrderedQuantity>
                        <Price>2000.0000</Price>
                        <ProductDescription>Aquilegia Vulgaris Barlow Blue</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>New Order</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>109034</VendorProductCode>
                    </PurchaseOrderItem>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>BELBRSD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>3</LineNumber>
                        <OrderedQuantity>2300.0000</OrderedQuantity>
                        <Price>2300.0000</Price>
                        <ProductDescription>Bellis Perennis Bellissima Red</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>New Order</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>109171</VendorProductCode>
                    </PurchaseOrderItem>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>DIGCASD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>4</LineNumber>
                        <OrderedQuantity>400.0000</OrderedQuantity>
                        <Price>400.0000</Price>
                        <ProductDescription>Digitalis Purpurea Camelot Rose</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>New Order</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>109434</VendorProductCode>
                    </PurchaseOrderItem>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>GAIAZSD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>5</LineNumber>
                        <OrderedQuantity>500.0000</OrderedQuantity>
                        <Price>500.0000</Price>
                        <ProductDescription>Gaillardia Grandiflora Arizona Sun</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>New Order</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>109615</VendorProductCode>
                    </PurchaseOrderItem>
                    <PurchaseOrderItem>
                        <AutoConfirm>true</AutoConfirm>
                        <BuyerProductCode>VIOPYSD</BuyerProductCode>
                        <Comment></Comment>
                        <ConfirmationNumber>A213975099081</ConfirmationNumber>
                        <CurrencyCode>USD</CurrencyCode>
                        <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                        <FreightCost i:nil="true" />
                        <FreightNotes i:nil="true" />
                        <LineId>1</LineId>
                        <LineNumber>6</LineNumber>
                        <OrderedQuantity>1500.0000</OrderedQuantity>
                        <Price>1500.0000</Price>
                        <ProductDescription>Viola Cornuta Princess Yellow</ProductDescription>
                        <ShipViaCode>DELIVERY</ShipViaCode>
                        <ShipViaDescription>DELIVERY</ShipViaDescription>
                        <ShipWeek>201737</ShipWeek>
                        <Size>URC</Size>
                        <Status>Cancelled</Status>
                        <Tax i:nil="true" />
                        <VendorProductCode>109616</VendorProductCode>
                    </PurchaseOrderItem>
                </Items>
                <Phone></Phone>
                <PostalCode>40361</PostalCode>
                <State>KY</State>
                <VendorCode>0000000040</VendorCode>
            </Load>
        </Loads>
        <OrderId>05902839-9f69-e711-9ff2-c4d987dc5de7</OrderId>
        <Revision>1</Revision>
        <RevisionId>3643a1ff-bf16-4955-9b5d-79ab9138e731</RevisionId>
        <TagRatio i:nil="true" />
    </PurchaseOrder>
</ArrayOfPurchaseOrder>

```

#### Upload Order ####
Allows the creation of a revision for a specific order

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder
* POST to this url
* Header requires Authorization and Content-Type
* Body should contain the purchaseorder xml

**Example Request**
```
POST https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 331
Accept: application/atom+xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer h5waAZX2IQzP3Vz9DZt2j3PDZ6m78Bw83MfH1we8i795WUkOeU3CloK67uX3zN5tzPAv6AGZGAtrDJjsUxuIUk4oE6u1FrrJTN1WsE8xN5oZ1OTSX5SB0YD214GwlPkUigC6BHDrGDezKcb5VVxFSRsZKheM3vsCfUScikQOuFHSd8Nqf7imITIhJdSwbu-jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

<PurchaseOrder xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <BrokerId>95d2c23c-7812-4783-a887-1da495c678ae</BrokerId>
    <ClientId>18abfe7c-903a-4427-9c8c-94dfc3ebf7c8</ClientId>
    <CreatedDate xmlns:d3p1="http://schemas.datacontract.org/2004/07/System">
        <d3p1:DateTime>2017-07-15T20:50:24.7785653Z</d3p1:DateTime>
        <d3p1:OffsetMinutes>-420</d3p1:OffsetMinutes>
    </CreatedDate>
    <CustomerPONumber>PO-1234</CustomerPONumber>
    <Loads>
        <Load>
            <Address1>1077 Cane Ridge Road</Address1>
            <Address2></Address2>
            <AddressCode>1234</AddressCode>
            <CareOf i:nil="true" />
            <City>Paris</City>
            <Country>US</Country>
            <Items>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>ALCCLSD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>1</LineNumber>
                    <OrderedQuantity>1000.0000</OrderedQuantity>
                    <Price>1000.0000</Price>
                    <ProductDescription>Alcea Rosea Spring Celebrities Lemon</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>New Order</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>107831</VendorProductCode>
                </PurchaseOrderItem>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>AQUBASD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>2</LineNumber>
                    <OrderedQuantity>2000.0000</OrderedQuantity>
                    <Price>2000.0000</Price>
                    <ProductDescription>Aquilegia Vulgaris Barlow Blue</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>New Order</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>109034</VendorProductCode>
                </PurchaseOrderItem>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>BELBRSD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>3</LineNumber>
                    <OrderedQuantity>2300.0000</OrderedQuantity>
                    <Price>2300.0000</Price>
                    <ProductDescription>Bellis Perennis Bellissima Red</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>New Order</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>109171</VendorProductCode>
                </PurchaseOrderItem>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>DIGCASD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>4</LineNumber>
                    <OrderedQuantity>400.0000</OrderedQuantity>
                    <Price>400.0000</Price>
                    <ProductDescription>Digitalis Purpurea Camelot Rose</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>New Order</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>109434</VendorProductCode>
                </PurchaseOrderItem>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>GAIAZSD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>5</LineNumber>
                    <OrderedQuantity>500.0000</OrderedQuantity>
                    <Price>500.0000</Price>
                    <ProductDescription>Gaillardia Grandiflora Arizona Sun</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>New Order</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>109615</VendorProductCode>
                </PurchaseOrderItem>
                <PurchaseOrderItem>
                    <AutoConfirm>true</AutoConfirm>
                    <BuyerProductCode>VIOPYSD</BuyerProductCode>
                    <Comment></Comment>
                    <ConfirmationNumber>A213975099081</ConfirmationNumber>
                    <CurrencyCode>USD</CurrencyCode>
                    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
                    <FreightCost i:nil="true" />
                    <FreightNotes i:nil="true" />
                    <LineId>1</LineId>
                    <LineNumber>6</LineNumber>
                    <OrderedQuantity>1500.0000</OrderedQuantity>
                    <Price>1500.0000</Price>
                    <ProductDescription>Viola Cornuta Princess Yellow</ProductDescription>
                    <ShipViaCode>DELIVERY</ShipViaCode>
                    <ShipViaDescription>DELIVERY</ShipViaDescription>
                    <ShipWeek>201737</ShipWeek>
                    <Size>URC</Size>
                    <Status>Cancelled</Status>
                    <Tax i:nil="true" />
                    <VendorProductCode>109616</VendorProductCode>
                </PurchaseOrderItem>
            </Items>
            <Phone></Phone>
            <PostalCode>40361</PostalCode>
            <State>KY</State>
            <VendorCode>0000000040</VendorCode>
        </Load>
    </Loads>
    <OrderId>05902839-9f69-e711-9ff2-c4d987dc5de7</OrderId>
    <Revision>1</Revision>
    <RevisionId>3643a1ff-bf16-4955-9b5d-79ab9138e731</RevisionId>
    <TagRatio i:nil="true" />
</PurchaseOrder>
```

#### Upload Order Item ####
Upload an order item. 

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder/{orderId}/{lineNumber}
* orderId - comes from the purchase order xml **OrderId** element
* lineNUmber - comes from the purchase order item xml **LineNumber** element
* POST to this url
* Header requires Authorization and Content-Type
* Body should contain the purchaseorderitem xml

**Example Request**
```
POST https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorder/7c84dc62-4fd9-4d30-925a-4d12a420e20a/1 HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 331
Accept: application/atom+xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer -jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

<PurchaseOrderItem xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <AutoConfirm>false</AutoConfirm>
    <BuyerProductCode>29608</BuyerProductCode>
    <Comment>Test Post of Line Item</Comment>
    <ConfirmationNumber i:nil="true" />
    <CurrencyCode i:nil="true" />
    <CutoffDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
    <ExpirationDate xmlns:d7p1="http://schemas.datacontract.org/2004/07/System" i:nil="true" />
    <FreightCost i:nil="true" />
    <FreightNotes i:nil="true" />
    <LineId>10398</LineId>
    <LineNumber>1</LineNumber>
    <OrderedQuantity>840.0000</OrderedQuantity>
    <Price>0.0000</Price>
    <ProductDescription>Salvia Nemorosa May Night</ProductDescription>
    <ShipViaCode>DELIVER</ShipViaCode>
    <ShipViaDescription>DELIVER</ShipViaDescription>
    <ShipWeek>201751</ShipWeek>
    <Size>URC</Size>
    <Status>New Order</Status>
    <Tax i:nil="true" />
    <VendorProductCode>065303</VendorProductCode>
</PurchaseOrderItem>
```

#### Acknowledge Order ####
Allows for the acknowledgement of the receipt of an order.  This will remove the order from subsequent calls to get new orders 

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorderacknowledgement
* POST to this url
* Header requires Authorization and Content-Type
* Body should contain the purchaseorderacknowledgement xml

**Example Request**
```
POST https://vendoravailability-qa.azurewebsites.net/api/v2/purchaseorderacknowledgement HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Content-Length: 331
Accept: application/atom+xml
Cache-Control: no-cache
Origin: chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop
Authorization: Bearer -jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Content-Type: application/xml
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Postman-Token: cb11bb98-afca-f6a2-3c84-70c17e01ed57
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

<PurchaseOrderAcknowledgement xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <Acknowledgements>
        <Acknowledgement>
            <Error>true</Error>
            <ErrorMessage>The Vendor Code is invalid</ErrorMessage>
            <RevisionId>8c3ab1b0-ba02-44e1-a82b-62899a718a7e</RevisionId>
        </Acknowledgement>
        <Acknowledgement>
            <Error>false</Error>
            <ErrorMessage i:nil="true" />
            <RevisionId>a19991ba-5c9a-4b56-8fde-b42f8d83db46</RevisionId>
        </Acknowledgement>
    </Acknowledgements>
</PurchaseOrderAcknowledgement>
```

#### Get Company List ####
Allows for the receipt of a list of companies that you do business with in beacon

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/company
* POST to this url
* Header requires Authorization and Content-Type

**Example Request**
```
GET https://vendoravailability-qa.azurewebsites.net/api/v2/company HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Accept: application/xml
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36
Authorization: Bearer -jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.8
```
**Example Response**
```
HTTP/1.1 200 OK
Content-Length: 188
Content-Type: application/xml; charset=utf-8
Server: Microsoft-IIS/10.0
Request-Context: appId=cid-v1:c23dce53-21ad-4cee-96f6-c55a6eaca160
X-Powered-By: ASP.NET
Date: Sun, 16 Jul 2017 04:15:43 GMT

<ArrayOfCompany xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <Company>
        <id>18abfe7c-903a-4427-9c8c-94dfc3ebf7c8</id>
        <name>Small Business Innovations</name>
    </Company>
</ArrayOfCompany>
```

#### Get Individual Company ####
Allows for the receipt of a specific company that you do business with in beacon

* Url: https://vendoravailability-qa.azurewebsites.net/api/v2/company/{id}
* POST to this url
* Header requires Authorization and Content-Type
* **id** is the ClientId found on PurchaseOrders

**Example Request**
```
GET https://vendoravailability-qa.azurewebsites.net/api/v2/company/18abfe7c-903a-4427-9c8c-94dfc3ebf7c8 HTTP/1.1
Host: vendoravailability-qa.azurewebsites.net
Connection: keep-alive
Accept: application/xml
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36
Authorization: Bearer -jw4b24VF3PRO2finGhWqRlT2nY2PrTcTWG21ajF6K-a9UKDzDcVqHyCPvgPzqty_FOHIwgyroYBqRW0cR_CS6WEVKqR3f5GuKC1qvJW6RSAl8J17oaO5kwxhBkRVlT8I1OTsbLJFZQ-1QFQcu0b_aX9SiYMOwdTDXUqI3m8miMavVjSrZ9Fv6_IsSmVrU0VwQYEM3ezTXeDu2gnW8_gG_JLg4P5Hy5NoLFYSPYR7YVZMllDmXM0iH91o1gZLckXwS5UJV5r7YcKxSY_HwPoar4lYMBzXUKnHuMnhVFoCgRvb62UDEKheNc_XSmI4HHWhV
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.8
```
**Example Response**
```
HTTP/1.1 200 OK
Content-Length: 188
Content-Type: application/xml; charset=utf-8
Server: Microsoft-IIS/10.0
Request-Context: appId=cid-v1:c23dce53-21ad-4cee-96f6-c55a6eaca160
X-Powered-By: ASP.NET
Date: Sun, 16 Jul 2017 04:15:43 GMT

<Company xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <id>18abfe7c-903a-4427-9c8c-94dfc3ebf7c8</id>
    <name>Small Business Innovations</name>
</Company>
```