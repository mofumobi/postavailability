﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Linq;

namespace Orders
{
    class Program
    {
        private const string username = "USERNAME_HERE";
        private const string password = "PASSWORD_HERE";
        static void Main(string[] args)
        {
            var authData = Authorize().Result;
            GetNewOrders(authData).Wait();
            var orders = GetAllOrders(authData).Result;
            AcknowledgeOrders(authData).Wait();
            PostNewOrder(authData).Wait();
            GetCompanyList(authData).Wait();
            GetCompany(authData).Wait();
            PostPurchaseOrderItem(authData, orders.FirstOrDefault()?.OrderId, orders.FirstOrDefault()?.Loads.FirstOrDefault()?.Items?.FirstOrDefault()).Wait();
            Console.ReadLine();
        }

        private static async Task<AuthRequest> Authorize()
        {
            var tokenUri = $"https://vendoravailability-qa.azurewebsites.net/token";
            var client = new HttpClient();
            var authDictionary = new Dictionary<string, string>();
            authDictionary.Add("username", username);
            authDictionary.Add("password", password);
            authDictionary.Add("grant_type", "password");
            var auth = new FormUrlEncodedContent(authDictionary);
            var authResponse = await client.PostAsync(tokenUri, auth);
            authResponse.EnsureSuccessStatusCode();
            return await authResponse.Content.ReadAsAsync<AuthRequest>();
        }

        private static async Task GetNewOrders(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var orderResponse = await client.GetAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/PurchaseOrder?onlyNew=true");

            var ordersXml = await orderResponse.Content.ReadAsStringAsync();
            var orders = await orderResponse.Content.ReadAsAsync<PurchaseOrder[]>();
            Console.WriteLine($"GetNewOrders: {orderResponse}");
        }

        private static async Task GetCompanyList(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var companyResponse = await client.GetAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/Company");

            var companies = await companyResponse.Content.ReadAsAsync<Company[]>();
            var companiesXml = await companyResponse.Content.ReadAsStringAsync();
            Console.WriteLine($"GetCompanyList: {companyResponse}");
        }

        private static async Task GetCompany(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var companyResponse = await client.GetAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/Company/18abfe7c-903a-4427-9c8c-94dfc3ebf7c8");

            var company = await companyResponse.Content.ReadAsAsync<Company>();
            var companyXml = await companyResponse.Content.ReadAsStringAsync();
            Console.WriteLine($"GetCompany: {companyResponse}");
        }

        private static async Task<PurchaseOrder[]> GetAllOrders(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var orderResponse = await client.GetAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/PurchaseOrder");

            var orders = await orderResponse.Content.ReadAsAsync<PurchaseOrder[]>(  );
            var ordersXml = await orderResponse.Content.ReadAsStringAsync();
            Console.WriteLine($"GetAllOrders: {orderResponse}");
            return orders;
        }

        private static async Task PostNewOrder(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var xmlData = File.ReadAllText(@"Data\PurchaseOrder.xml");
            var purchaseOrder = new StringContent(xmlData, Encoding.UTF8, "application/xml");
            var orderResponse = await client.PostAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/PurchaseOrder", purchaseOrder);

            var ordersXml = await orderResponse.Content.ReadAsStringAsync();
            Console.WriteLine($"PostNewOrder: {orderResponse}");
        }

        private static async Task AcknowledgeOrders(AuthRequest authData)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/xml");
            var xmlData = File.ReadAllText(@"Data\PurchaseOrderAcknowledgement.xml");
            var purchaseOrderAcknowledgement = new StringContent(xmlData, Encoding.UTF8, "application/xml");
            var acknowledgementResponse = await client.PostAsync($"https://vendoravailability-qa.azurewebsites.net/api/v2/PurchaseOrderAcknowledgement", purchaseOrderAcknowledgement);
            
            Console.WriteLine($"AcknowledgeOrders: {acknowledgementResponse}");
        }

        private static async Task PostPurchaseOrderItem(AuthRequest authData, string orderId, PurchaseOrderItem purchaseOrderItem)
        {
            if (purchaseOrderItem != null)
            {
                var itemUri = $"https://vendoravailability-qa.azurewebsites.net/api/v2/PurchaseOrder/{orderId}/{purchaseOrderItem.LineNumber}";
                purchaseOrderItem.Comment = "Test Post of Line Item";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authData.access_token);
                var purchaseOrderItemResponse = await client.PostAsXmlAsync(itemUri, purchaseOrderItem);
                Console.WriteLine($"purchaseOrderItem Response: {purchaseOrderItemResponse.StatusCode}");
            }
            else
            {
                Console.WriteLine($"purchaseOrderItem Response: No Orders");
            }
        }
    }
}
